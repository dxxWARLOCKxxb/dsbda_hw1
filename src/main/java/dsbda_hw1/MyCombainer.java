package dsbda_hw1;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.Reducer;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;

public class MyCombainer extends MapReduceBase implements Reducer<Text, Text, Text, Text> {

    @Override
    public void reduce(Text agent, Iterator<Text> values, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
        HashSet<Text> set = new HashSet<>();

        // Отберем уникальные IP адреса

        while(values.hasNext()) {
            set.add(values.next());
        }

        // Зафиксируем в статистике количество отправлленых на Reducer данных

        reporter.getCounter("Combiners statistics", agent.toString()).increment(set.size());

        // Отправим на Reducer уникальные ip-адреса

        for (Text text : set) {
            output.collect(agent, text);
        }
    }
}
