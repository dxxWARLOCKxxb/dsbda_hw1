package dsbda_hw1;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;

public class MyReducer extends MapReduceBase implements Reducer<Text, Text, Text, LongWritable> {

    @Override
    public void reduce(Text agent, Iterator<Text> IPs, OutputCollector<Text, LongWritable> output, Reporter reporter) throws IOException {

        HashSet<Text> users = new HashSet<>();

        // Оставим уникальные ip адреса в разрезе агента (Combiner работает локально)

        while (IPs.hasNext()) {
            users.add(IPs.next());
        }

        // В ответ запишем количество уникальных ip-адресов (пользователей)
        output.collect(agent, new LongWritable(users.size()));
    }
}