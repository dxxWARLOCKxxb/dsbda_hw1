package dsbda_hw1;

import org.apache.hadoop.util.ToolRunner;

public class Main {
    public static void main(String[] args) throws Exception {
        int exitCode = ToolRunner.run(new MyCounter(), args);
        System.exit(exitCode);
    }
}
