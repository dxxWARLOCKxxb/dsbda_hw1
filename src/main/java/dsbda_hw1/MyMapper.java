package dsbda_hw1;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

import java.io.IOException;

public class MyMapper extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> {

    @Override
    public void map(LongWritable key, Text value, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {

        String nextString = value.toString().toLowerCase();
        String ip = nextString.split(" ")[0];

        // Зафиксируем в статистике количество строк журнала, в которых обнаружен Firefox, IE или прочие агенты
        // Отправим на Combiner ip-адреса сгруппированные по агенту

        if (nextString.contains("firefox")) {
            reporter.getCounter("Mappers statistics", "Firefox").increment(1);

            output.collect(new Text("Firefox"), new Text(ip));
        }
        else if (nextString.contains("msie")) {
            reporter.getCounter("Mappers statistics", "IE").increment(1);
            output.collect(new Text("Internet Explorer"), new Text(ip));
        }
        else {
            reporter.getCounter("Mappers statistics", "Other").increment(1);
        }
    }
}

