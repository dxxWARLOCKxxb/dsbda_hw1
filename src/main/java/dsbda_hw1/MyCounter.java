package dsbda_hw1;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.Tool;

public class MyCounter extends Configured implements Tool {

    @Override
    public int run(String[] args) throws Exception {
        String input, output;

        // Если в аргументах не указаны входной/выходной каталоги, установим стандартные значения.

        if(args.length == 2) {
            input = args[0];
            output = args[1];
        } else {
            input = "input";
            output = "output";
        }

        JobConf conf = new JobConf(getConf(), MyCounter.class);

        conf.setJobName(this.getClass().getName());

        // Настроим CSV выходной формат.

        conf.setOutputFormat(TextOutputFormat.class);
        conf.set("mapreduce.output.textoutputformat.separator", ";");

        FileInputFormat.setInputPaths(conf, new Path(input));
        FileOutputFormat.setOutputPath(conf, new Path(output));

        conf.setMapperClass(MyMapper.class);
        conf.setMapOutputKeyClass(Text.class);
        conf.setMapOutputValueClass(Text.class);

        conf.setCombinerClass(MyCombainer.class);

        conf.setReducerClass(MyReducer.class);
        conf.setOutputKeyClass(Text.class);
        conf.setOutputValueClass(LongWritable.class);

        conf.setNumReduceTasks(2);

        RunningJob job = JobClient.runJob(conf);

        // Соберем статистику по отправленным данным с пользовательских Counters

        long firefox_r = job.getCounters().findCounter("Mappers statistics", "Firefox").getValue();
        long ie_r = job.getCounters().findCounter("Mappers statistics", "IE").getValue();
        long other_r = job.getCounters().findCounter("Mappers statistics", "Other").getValue();
        long firefox_u = job.getCounters().findCounter("Combiners statistics", "Firefox").getValue();
        long ie_u = job.getCounters().findCounter("Combiners statistics", "Internet Explorer").getValue();

        System.out.println("Mappers statistics:");
        System.out.println("\tFirefox: " + firefox_r);
        System.out.println("\tInternet Explorers: " + ie_r);
        System.out.println("\tOthers: " + other_r);

        System.out.println("Combiners statistics:");
        System.out.println("\tFirefox: " + firefox_u);
        System.out.println("\tInternet Explorers: " + ie_u);

        return 0;
    }
}
