#!/bin/sh

echo "Format node"
/opt/hadoop-2.9.1/bin/hdfs namenode -format

echo "Start DFS"
/opt/hadoop-2.9.1/sbin/start-dfs.sh

echo "Create user folder"
/opt/hadoop-2.9.1/bin/hdfs dfs -mkdir /user
/opt/hadoop-2.9.1/bin/hdfs dfs -mkdir /user/daria

echo "Copy input files"
/opt/hadoop-2.9.1/bin/hdfs dfs -put ./input input

