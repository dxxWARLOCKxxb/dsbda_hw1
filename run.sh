#!/bin/sh

echo "Delete local output data"
rm -rf ./output
rm result.csv

echo "Delete hdfs output folder"
/opt/hadoop-2.9.1/bin/hdfs dfs -rm -r output

echo "Start app"
/opt/hadoop-2.9.1/bin/hadoop jar ./target/homework1-1.0-SNAPSHOT-jar-with-dependencies.jar

echo "Get result"
/opt/hadoop-2.9.1/bin/hdfs dfs -get output output
cat output/* > result.csv

echo "Result is: "
cat result.csv


